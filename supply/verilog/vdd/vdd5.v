(* blackbox *)
module VDD5 (output out);
   supply1 vdd;

   assign out = vdd;
endmodule
