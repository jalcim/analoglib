(* blackbox *)
module VDD2_5 (output out);
   supply1 vdd;

   assign out = vdd;
endmodule
