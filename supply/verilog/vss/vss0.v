(* blackbox *)
module VSS0 (output out);
   supply0 vss;

   assign out = vss;
endmodule
