(* blackbox *)
module resistor_1ohm (out, in);
   input in;
   output out;

   assign out = in;
endmodule
