(* capacitor *)
module capacitor_1nf(out, in);
   input in;
   output out;

   assign out = in;
endmodule
